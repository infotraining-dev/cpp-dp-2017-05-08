#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>
#include <sstream>

//enum AccountState { OVERDRAFT, NORMAL };

class BankAccount
{
    class AccountState
    {
    public:
        virtual void withdraw(double amount, BankAccount& ba) const = 0;
        virtual void pay_interest(BankAccount& ba) const = 0;
        virtual std::string status() const = 0;
        virtual ~AccountState() = default;
    };

    class NormalState : public AccountState
    {
        static constexpr const char* id = "Normal";
        static constexpr double interest_rate = 0.05;

        // AccountState interface
    public:
        void withdraw(double amount, BankAccount& ba) const override;
        void pay_interest(BankAccount& ba) const override;
        std::string status() const override;
    };

    class OverdraftState : public AccountState
    {
        static constexpr const char* id = "Overdraft";
        static constexpr double interest_rate = 0.15;
        // AccountState interface
    public:
        void withdraw(double amount, BankAccount& ba) const override;
        void pay_interest(BankAccount& ba) const override;
        std::string status() const override;
    };

	int id_;
	double balance_;
    AccountState const* state_;

    static const NormalState NORMAL;
    static const OverdraftState OVERDRAFT;
protected:
	void update_account_state()
	{
		if (balance_ < 0)
            state_ = &OVERDRAFT;
		else
            state_ = &NORMAL;
	}

	void set_balance(double amount)
	{
		balance_ = amount;
	}
public:
    BankAccount(int id) : id_(id), balance_(0.0), state_(&NORMAL) {}

	void withdraw(double amount)
	{
		assert(amount > 0);

        state_->withdraw(amount, *this);

        update_account_state();
	}

	void deposit(double amount)
	{
		assert(amount > 0);

		balance_ += amount;

		update_account_state();
	}

	void pay_interest()
	{
        state_->pay_interest(*this);
	}

    std::string status() const
	{
        std::stringstream strm;
        strm << "BankAccount #" << id_ << "; State: ";

        strm << state_->status();

        strm << "Balance: " << balance_;

        return strm.str();
	}

	double balance() const
	{
		return balance_;
	}

	int id() const
	{
		return id_;
	}
};

#endif

void BankAccount::NormalState::withdraw(double amount, BankAccount& ba) const
{
    ba.balance_ -= amount;
}

void BankAccount::NormalState::pay_interest(BankAccount& ba) const
{
    ba.balance_ += ba.balance_ * BankAccount::NormalState::interest_rate;
}

std::string BankAccount::NormalState::status() const
{
    return BankAccount::NormalState::id;
}

void BankAccount::OverdraftState::withdraw(double, BankAccount& ba) const
{
    std::cout << "Brak srodkow na koncie #" << ba.id_ <<  std::endl;
}

void BankAccount::OverdraftState::pay_interest(BankAccount& ba) const
{
    ba.balance_ += ba.balance_ * BankAccount::OverdraftState::interest_rate;
}

std::string BankAccount::OverdraftState::status() const
{
    return BankAccount::OverdraftState::id;
}
