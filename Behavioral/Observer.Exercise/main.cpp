#include "stock.hpp"

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
    Stock nokia("NOKIA", 95.0);

    Investor kulczyk_jr{"Kulczyk Jr"};
    Investor solorz_jr{"Solorz Jr"};

    nokia.connect([&kulczyk_jr](auto s, auto p) { kulczyk_jr.update(s, p); });
    auto conn1 = nokia.connect([&solorz_jr](auto s, auto p) { solorz_jr.update(s, p); });

	// zmian kurs�w
	misys.set_price(360.0);
	ibm.set_price(210.0);
    nokia.set_price(145.0);

    cout << "\n\n";
    conn1.disconnect();

	misys.set_price(380.0);
	ibm.set_price(230.0);
    nokia.set_price(155.0);
}
