#include "strategy.hpp"

int main()
{
    Context context { ConcreteStrategyA() };
    context.context_interface();

    auto s2 = ConcreteStrategyB();
    context.reset_strategy(s2);
    context.context_interface();

    context.reset_strategy(&concrete_strategy_c);
    context.context_interface();
}
