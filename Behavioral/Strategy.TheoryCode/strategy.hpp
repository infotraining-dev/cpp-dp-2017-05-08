#ifndef STRATEGY_HPP_
#define STRATEGY_HPP_

#include <iostream>
#include <memory>
#include <algorithm>
#include <functional>
#include <cstring>

// "Strategy"
//class Strategy
//{
//public:
//    virtual void algorithm_interface(const std::string& data) = 0;
//    virtual ~Strategy() = default;
//};

using Strategy = std::function<void (const std::string&)>;

// "ConcreteStrategyA"
class ConcreteStrategyA
{
public:
    void operator()(const std::string& data)
    {
        std::string transformed_data{data};

        std::transform(data.begin(), data.end(), transformed_data.begin(), [](char c) { return std::toupper(c); });

        std::cout << "Data: " <<  transformed_data << "\n";
    }
};

// "ConcreteStrategyB"
class ConcreteStrategyB
{
public:
    virtual void operator()(const std::string& data)
    {
        std::string transformed_data{data};

        std::transform(data.begin(), data.end(), transformed_data.begin(), [](char c) { return std::tolower(c); });

        std::cout << "Data: " <<  transformed_data << "\n";
    }
};

// "ConcreteStrategyC"
void concrete_strategy_c(const std::string& data)
{
    std::string transformed_data{data};

    if (data.size() >= 1)
    {
        transformed_data.front() = std::toupper(data.front());

        std::transform(data.begin() + 1, data.end(), transformed_data.begin() + 1, [](char c) { return std::tolower(c); });
    }

    std::cout << "Data: " <<  transformed_data << "\n";
}


// "Context" 
class Context
{
    Strategy strategy_;
    std::string data_ = "text";

public:
    Context(Strategy strategy) : strategy_{strategy}
    {
    }

    void reset_strategy(Strategy new_strategy)
    {
        strategy_ = new_strategy;
    }

    void context_interface()
    {
        strategy_(data_);
    }

    std::string data() const
    {
        return data_;
    }

    void set_data(const std::string &data)
    {
        data_ = data;
    }

};

#endif /*STRATEGY_HPP_*/
