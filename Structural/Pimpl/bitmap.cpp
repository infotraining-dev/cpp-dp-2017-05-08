#include "bitmap.hpp"
#include <array>

using namespace std;

struct Bitmap::BitmapImpl
{
    array<char, 15> image_;
};

Bitmap::Bitmap(char fill_char) : impl_{make_unique<BitmapImpl>()}
{
    impl_->image_.fill(fill_char);
}

Bitmap::~Bitmap() = default;

void Bitmap::draw()
{
    for(const auto pixel : impl_->image_)
        cout << pixel;
    cout << endl;
}
