# README #

## Ankieta ##

* https://docs.google.com/a/infotraining.pl/forms/d/e/1FAIpQLSeCPVYKjALZYLr547OgVsR7pYtnuaY0msSFr8Z9mb-mm6FXoQ/viewform?hl=pl

## Ustawienia proxy (VM)

Dodać na końcu pliku `.profile`:

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```