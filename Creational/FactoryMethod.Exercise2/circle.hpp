#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.hpp"

namespace Drawing
{
    class Circle : public ShapeBase
    {
        int radius_;
    public:
        static constexpr const char* id = "Circle";

        Circle(int x = 0, int y = 0, int r = 0);
        void draw() const override;
        int radius() const;
        void set_radius(int radius);
    };
}

#endif // CIRCLE_H
