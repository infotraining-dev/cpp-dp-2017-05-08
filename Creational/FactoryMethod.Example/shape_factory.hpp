#ifndef SHAPE_FACTORY_HPP
#define SHAPE_FACTORY_HPP

#include <string>
#include <functional>
#include <memory>
#include <unordered_map>

#include "shape.hpp"

template<
    typename ProductType,
    typename IndexType = std::string,
    typename CreatorType = std::function<std::unique_ptr<ProductType>()>
>
class Factory
{
    std::unordered_map<IndexType, CreatorType> creators_;
public:
    std::unique_ptr<ProductType> create(IndexType index)
    {
        return creators_.at(index)();
    }

    bool register_creator(IndexType index, CreatorType creator)
    {
        return creators_.insert(make_pair(index, creator)).second;
    }
};

using ShapeFactory = Factory<Drawing::Shape>;

#endif // SHAPE_FACTORY_HPP
