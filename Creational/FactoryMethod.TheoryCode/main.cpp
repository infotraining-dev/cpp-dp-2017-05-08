#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>

#include "factory.hpp"

using namespace std;

namespace Canonical
{

    class Client
    {
        shared_ptr<ServiceCreator> creator_;
    public:
        Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
        {
        }

        Client(const Client&) = delete;
        Client& operator=(const Client&) = delete;

        void use()
        {
            unique_ptr<Service> service = creator_->create_service();

            string result = service->run();
            cout << "Client is using: " << result << endl;
        }
    };

    typedef std::map<std::string, shared_ptr<ServiceCreator>> Factory;
}


class Client
{
    ServiceCreator creator_;
public:
    Client(ServiceCreator creator) : creator_(creator)
    {
    }

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
    {
        unique_ptr<Service> service = creator_();

        string result = service->run();
        cout << "Client is using: " << result << endl;
    }
};

using Factory = std::map<std::string, ServiceCreator>;


struct ConcreteCreatorC
{
    unique_ptr<Service> operator()() const
    {
        return make_unique<ConcreteServiceC>();
    }
};

int main()
{
//    using namespace Canonical;

//    Factory creators;
//    creators.insert(make_pair("ServiceA", make_shared<ConcreteCreatorA>()));
//    creators.insert(make_pair("ServiceB", make_shared<ConcreteCreatorB>()));
//    creators.insert(make_pair("ServiceC", make_shared<ConcreteCreatorC>()));

    Factory creators;
    creators.insert(make_pair("ServiceA", [] { return make_unique<ConcreteServiceA>(); }));
    creators.insert(make_pair("ServiceB", &make_unique<ConcreteServiceB>));
    creators.insert(make_pair("ServiceC", ConcreteCreatorC{}));

    Client client(creators["ServiceC"]);
    client.use();
}
